//Bank html elements
const BankDivContainer = document.getElementById("Bank")
const BalanceDisplayElement = document.getElementById("BalanceDisplay");
const LoanButtonElement = document.getElementById("LoanButton");

//Work html elements
const WorkButtonElement = document.getElementById("WorkButton");
const BankButtonElement = document.getElementById("BankButton");
const workDivContainer = document.getElementById("Work");
const AccumulatedPayElementDisplay = document.getElementById("AccumulatedPay");

//Computer html Elements 
const ComputerSelectionPanel = document.getElementById("ComputerSelectionPanel");
const ComputerSelectorElement = document.getElementById("ComputerSelector");
const ComputerFeatureElement = document.getElementById("Features");
const ComputerImgElement = document.getElementById("ComputerImageViewer");
const ComputerTitleElement = document.getElementById("ComputerTitle");
const ComputerDescriptionElement = document.getElementById("ComputerDescription");
const ComputerPriceElement = document.getElementById("ComputerPrice");
const BuyButtonElement = document.getElementById("BuyButton");

let loanAmount = 0;
let HasLoan = { booleanValue: false };
let workPay = {Pay: 0}

//create an unAppended DOM element for loan button
let RepayLoanButtonElement = document.createElement("button");
    RepayLoanButtonElement.id = "RepayLoanButton";
    RepayLoanButtonElement.innerText = "Repay Loan with pay";

//create an unappended DOM element for loan display and its currency convetion
let LoanDisplayParagraph = document.createElement("p");
LoanDisplayParagraph.id = "LoanDisplay";
let LoanNok = document.createElement("p");
LoanNok.id = "LoanNok";
LoanNok.innerText = "NOK (Loan)";

//fetching endpoint data and storing them to the computers array
let Computers = [];
fetch("https://hickory-quilled-actress.glitch.me/computers")
    .then(response => response.json())
    .then(data => Computers = data)
    .then(Computers => addComputersToSelector(Computers));
    
    //adding individual computer to array of computers
    // shows the first computer element when page is loaded
    const addComputersToSelector = (Computers) => 
    {
        Computers.forEach(x => addComputerToSelector(x));
        ComputerFeatureElement.innerText = Computers[0].specs;
        ComputerTitleElement.innerText = Computers[0].title;
        ComputerImgElement.src = "https://hickory-quilled-actress.glitch.me" + "/" + Computers[0].image;
        ComputerDescriptionElement.innerText = Computers[0].description;
        ComputerPriceElement.innerText = Computers[0].price;
    }
    //create an option element for each computer
    const addComputerToSelector = (Computer) =>
    {
        const ComputerElement = document.createElement("option");
        ComputerElement.value = Computer.id;
        ComputerElement.appendChild(document.createTextNode(Computer.title));
        ComputerSelectorElement.appendChild(ComputerElement);
    }

    //create a handler that selects and stores each individual computer info to html element
    const handleComputerSelectorChange = e =>
    {
        const selectedComputer = Computers[e.target.selectedIndex]
        ComputerFeatureElement.innerText = selectedComputer.specs;
        ComputerTitleElement.innerText = selectedComputer.title;
        ComputerImgElement.src = "https://hickory-quilled-actress.glitch.me" + "/" + imgRouteChecker(selectedComputer);
        ComputerDescriptionElement.innerText = selectedComputer.description;
        ComputerPriceElement.innerText = selectedComputer.price;
    }
    //calls an eventlistener for handleComputerSelectorChange
    ComputerSelectorElement.addEventListener("click", handleComputerSelectorChange);
    
    //creates a imgRouteChecker specifically made for image 5
    let imgRouteChecker = (selectedComputer) =>
    {
        const RoutedImgLink = "assets/images/5.jpg"
        if(selectedComputer.image === RoutedImgLink)
        {
            selectedComputer.image = "assets/images/5.png"
            return selectedComputer.image
        }
        else
        return selectedComputer.image
    }

WorkButtonElement.addEventListener("click", () =>
{
    workPay.Pay += 100;
    AccumulatedPayElementDisplay.innerText = workPay.Pay;
});

LoanButtonElement.addEventListener("click", () =>
{
    if(HasLoan.booleanValue == true)
    {
        alert("how dare you to ask for a second loan!! pay back your current loan and we may grant you another after that")
    }
    else
    {
        //checks if loan is a number and gives a loan based on the if statements
        let loanAmountRequest = prompt("how much loan do you want?")
        loanAmount = parseInt(loanAmountRequest); 
        if(!isNaN(loanAmount)) 
        {
            if (loanAmount > (2*BalanceDisplayElement.innerText))
            {
                alert("you are to poor for this loan, please enter a value less than double of your current balance")
            }
            else if (HasLoan.booleanValue == false)
            {
                LoanDisplayParagraph.innerText = loanAmount;
                BankDivContainer.appendChild(LoanDisplayParagraph);
                BankDivContainer.appendChild(LoanNok);
                HasLoan.booleanValue = true;
                workDivContainer.appendChild(RepayLoanButtonElement);
                let balanceNumber = parseInt(BalanceDisplayElement.innerText)
                BalanceDisplayElement.innerText = loanAmount + balanceNumber;
            }
        }
        else
        {
            alert("Enter a number pretty please");
        }
    }
});

BankButtonElement.addEventListener("click", () =>
{
    SendPayToBank(workPay,HasLoan);
    workPay.Pay = 0;
    AccumulatedPayElementDisplay.innerText = workPay.Pay;
});

 //repays with the full amount of work pay. if the pay is over the loan the remaining will be transfered to the balance
 RepayLoanButtonElement.addEventListener("click", () =>
 {
     loanAmount -= workPay.Pay
     workPay.Pay = 0;
     AccumulatedPayElementDisplay.innerText = workPay.Pay;
     LoanDisplayParagraph.innerText = loanAmount;
     if (loanAmount <= 0)
     {
         BalanceDisplayElement.innerText = parseInt(BalanceDisplayElement.innerText) + (-1*loanAmount); 
         loanAmount = 0;
         LoanDisplayParagraph.innerText = loanAmount; 
         HasLoan.booleanValue = false;
         RepayLoanButtonElement.remove()
         LoanDisplayParagraph.remove();
         LoanNok.remove()
     }
 });

 BuyButtonElement.addEventListener("click", () =>
 {
    let balanceNumber = parseInt(BalanceDisplayElement.innerText)
    let selectedComputerPrice = parseInt(ComputerPriceElement.innerText)
    if (!isNaN(balanceNumber) && !isNaN(selectedComputerPrice) && balanceNumber < selectedComputerPrice)
    {
        alert("you are to poor for this computer, take a loan or something!!")
    }
    else
    {
        BalanceDisplayElement.innerText = (balanceNumber - selectedComputerPrice)
        setTimeout(function(){ alert("congratulations with your new computer, i hope you feel better about yourself :)"); }, 200);
    }
 });


//will send money to bank after a 10% deduction to the loan if a loan has been granted
// the remaining deduction after the loan is payed, is transfered to the bank balance 
let SendPayToBank = (workPay,HasLoan) =>
    {
        if(HasLoan.booleanValue === true && loanAmount > 0)
        {
            loanAmount -= (workPay.Pay*0.1)
            LoanDisplayParagraph.innerText = loanAmount;
            let balanceDisplayNumber = parseInt(BalanceDisplayElement.innerText)
            if(!isNaN(balanceDisplayNumber))
            {
                let resultOfBalanceAndLoanAndPay = balanceDisplayNumber + (workPay.Pay*0.9);

                BalanceDisplayElement.innerText = resultOfBalanceAndLoanAndPay;
            }
            if (loanAmount <= 0)
            {
                BalanceDisplayElement.innerText = balanceDisplayNumber + (-1*loanAmount) + (workPay.Pay*0.9);
                loanAmount = 0;
                HasLoan.booleanValue = false;
                TempLoanDisplay.remove();
                RepayLoanButtonElement.remove();
                LoanDisplayParagraph.remove();
                LoanNok.remove()
            }
            else
            {
                LoanDisplayParagraph.innerText = loanAmount;
            }
        }
        else
        {
            if(!isNaN(BalanceDisplayElement.innerText))
            {
                let balanceAndworkPay = parseInt(BalanceDisplayElement.innerText) + workPay.Pay;
                BalanceDisplayElement.innerText = balanceAndworkPay;
            }
        }
    }